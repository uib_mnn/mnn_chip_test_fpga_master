# CHIP TEST FPGA MASTER
Este proyecto se ha hecho para comprobar el primer chip que hemos enviado a fabricar (MNN de 32n). 
La idea es que utilizando el kit de FPGA DE10-nano sirva de `master` para enviar al chip la información, activar la inferencia y leer los resultados. 
Dentro de la FPGA, solo utilizaremos el `SoC` para controlar las señales necesarias para el chip. 

# CAMBIO DE FRECUENCIA DE RELOJ DE CHIP
Podemos cambiar la frecuencia de reloj del chip modificando el fichero [`init.txt`](Soc/Input/init.txt) del `SoC`. 
Donde el valor de `freq_clk ` puede ser:

| freq_clk | Frecuencia      |
|----------|-----------------|
| 0        | 25 MHz          |
| 1        | 12.5 MHz        |
| 2        | 6.25 MHz        |
| 3        | 3.125 MHz       |
| 4        | 1.5625 MHz      |
| 5        | 781.25 kHz      |
| 6        | 390.625 kHz     |
| 7        | 195.3125 kHz    |
| 8        | 97.65625 kHz    |
| 9        | 48.828125 kHz   |
| 10       | 24.4140625 kHz  |
| 11       | 12.20703125 kHz |
| 12       | 6.103515625 kHz |
| 13       | 3.0517578125 kHz|
| 14       | 1.52587890625 kHz|
| 15       | 762.939453125 Hz|
| 16       | 381.4697265625 Hz|
