
//=======================================================
//  This code is generated by Terasic System Builder
//=======================================================

module DE10_NANO_SoC_GHRD(

    
     
     //////////// CLOCK //////////
    input               FPGA_CLK1_50,
    input               FPGA_CLK2_50,
    input               FPGA_CLK3_50,

    //////////// HDMI //////////
    inout               HDMI_I2C_SCL,
    inout               HDMI_I2C_SDA,
    inout               HDMI_I2S,
    inout               HDMI_LRCLK,
    inout               HDMI_MCLK,
    inout               HDMI_SCLK,
    output              HDMI_TX_CLK,
    output   [23: 0]    HDMI_TX_D,
    output              HDMI_TX_DE,
    output              HDMI_TX_HS,
    input               HDMI_TX_INT,
    output              HDMI_TX_VS,

    //////////// HPS //////////
    inout               HPS_CONV_USB_N,
    output   [14: 0]    HPS_DDR3_ADDR,
    output   [ 2: 0]    HPS_DDR3_BA,
    output              HPS_DDR3_CAS_N,
    output              HPS_DDR3_CK_N,
    output              HPS_DDR3_CK_P,
    output              HPS_DDR3_CKE,
    output              HPS_DDR3_CS_N,
    output   [ 3: 0]    HPS_DDR3_DM,
    inout    [31: 0]    HPS_DDR3_DQ,
    inout    [ 3: 0]    HPS_DDR3_DQS_N,
    inout    [ 3: 0]    HPS_DDR3_DQS_P,
    output              HPS_DDR3_ODT,
    output              HPS_DDR3_RAS_N,
    output              HPS_DDR3_RESET_N,
    input               HPS_DDR3_RZQ,
    output              HPS_DDR3_WE_N,
    output              HPS_ENET_GTX_CLK,
    inout               HPS_ENET_INT_N,
    output              HPS_ENET_MDC,
    inout               HPS_ENET_MDIO,
    input               HPS_ENET_RX_CLK,
    input    [ 3: 0]    HPS_ENET_RX_DATA,
    input               HPS_ENET_RX_DV,
    output   [ 3: 0]    HPS_ENET_TX_DATA,
    output              HPS_ENET_TX_EN,
    inout               HPS_GSENSOR_INT,
    inout               HPS_I2C0_SCLK,
    inout               HPS_I2C0_SDAT,
    inout               HPS_I2C1_SCLK,
    inout               HPS_I2C1_SDAT,
    inout               HPS_KEY,
    inout               HPS_LED,
    inout               HPS_LTC_GPIO,
    output              HPS_SD_CLK,
    inout               HPS_SD_CMD,
    inout    [ 3: 0]    HPS_SD_DATA,
    output              HPS_SPIM_CLK,
    input               HPS_SPIM_MISO,
    output              HPS_SPIM_MOSI,
    inout               HPS_SPIM_SS,
    input               HPS_UART_RX,
    output              HPS_UART_TX,
    input               HPS_USB_CLKOUT,
    inout    [ 7: 0]    HPS_USB_DATA,
    input               HPS_USB_DIR,
    input               HPS_USB_NXT,
    output              HPS_USB_STP,

    //////////// KEY //////////
    input    [ 1: 0]    KEY,

    //////////// LED //////////
    output   [ 7: 0]    LED,

    //////////// SW //////////
    input    [ 3: 0]    SW,
     // Chip pinouts
    output              chip_clk,
    output   [ 7: 0]    chip_i_data,
    output              chip_i_wren,
    output              chip_i_rst,
    output              chip_i_en,
    output   [ 3: 0]    chip_i_add,
    input    [ 7: 0]    chip_o_result,
    input               chip_o_done     
);



//=======================================================
//  REG/WIRE declarations
//=======================================================
wire hps_fpga_reset_n;
// pio
(* keep *) wire     [1: 0]     fpga_debounced_buttons;
(* keep *) wire     [6: 0]     fpga_led_internal;
// My signals interface

(* keep *) wire     [7: 0]  i_fsm_cmnd_wr;
(* keep *) wire     [31: 0] i_fsm_add;
(* keep *) wire     [31: 0] o_fsm_aux1n;
(* keep *) wire     [31: 0] o_fsm_aux2n;
(* keep *) wire             o_fsm_busy;
(* keep *) wire     [7: 0]  i_fsm_cmnd_ctrl;
(* keep *) wire     [7: 0]  o_fsm_cmnd_rd;
(* keep *) wire     [31: 0] o_fsm_data_rd;
(* keep *) wire     [31: 0] i_fsm_data_wr;
(* keep *) wire             i_fsm_wr_en; 


//(* keep *) wire     [7:0]      sram_control_pio;
(* keep *) wire     [7:0]      control_pio;    
// onchip sram s2 exported to the FPGA fabric
(* keep *) wire [13:0]   sram_address_sig;
(* keep *) wire          sram_chipselect_sig; 
(* keep *) wire          sram_clken_sig;    
(* keep *) wire          sram_write_sig;    
(* keep *) wire [31:0]   sram_readdata_sig;
(* keep *) wire [31:0]   sram_writedata_sig;
(* keep *) wire [3:0]    sram_byteenable_sig; 
// additional signals for the accelerator
//wire reset_sig;
//wire enable_sig;
//wire startX_sig;
//wire startW_sig;
//wire startComputation_sig;
wire ready_sig;
wire [15:0] input_size_sig;
wire [15:0] output_size_sig;
// other (hps stuff)
wire     [2: 0]     hps_reset_req;
wire                hps_cold_reset;
wire                hps_warm_reset;
wire                hps_debug_reset;
wire     [27: 0]    stm_hw_events;
// other (clocks and pll signals)
wire                fpga_clk_50;
wire                fpga_clk_slow;
wire                clock_choice;
wire                pll_locked;
// connection of internal logics
assign LED[7: 1] = fpga_led_internal;
assign fpga_clk_50 = FPGA_CLK2_50;
assign stm_hw_events = {{15{1'b0}}, SW, fpga_led_internal, fpga_debounced_buttons};

// test para debug
wire 	[7:0]		p2b_x0_bin;
wire 	[7:0]		p2b_w0_bin;
wire 	[7:0]		p2b_m0_bin;
wire  [7:0]		db_lfsr_in;



//=======================================================
//  Structural coding
//=======================================================

// Clocks
//pll_50_to_25 upll
//(
//	.refclk   (fpga_clk_50),
//	.rst      (hps_fpga_reset_n),
//	.outclk_0 (fpga_clk_slow),
//	.locked   (pll_locked),
//);







// Computer System
soc_system u0(
               //Clocks&Reset
               .clk_clk(fpga_clk_50),                                       //                         clk.clk
               .reset_reset_n(hps_fpga_reset_n),                            //                         reset.reset_n
               //HPS ddr3
               .memory_mem_a(HPS_DDR3_ADDR),                                //                         memory.mem_a
               .memory_mem_ba(HPS_DDR3_BA),                                 //                               .mem_ba
               .memory_mem_ck(HPS_DDR3_CK_P),                               //                               .mem_ck
               .memory_mem_ck_n(HPS_DDR3_CK_N),                             //                               .mem_ck_n
               .memory_mem_cke(HPS_DDR3_CKE),                               //                               .mem_cke
               .memory_mem_cs_n(HPS_DDR3_CS_N),                             //                               .mem_cs_n
               .memory_mem_ras_n(HPS_DDR3_RAS_N),                           //                               .mem_ras_n
               .memory_mem_cas_n(HPS_DDR3_CAS_N),                           //                               .mem_cas_n
               .memory_mem_we_n(HPS_DDR3_WE_N),                             //                               .mem_we_n
               .memory_mem_reset_n(HPS_DDR3_RESET_N),                       //                               .mem_reset_n
               .memory_mem_dq(HPS_DDR3_DQ),                                 //                               .mem_dq
               .memory_mem_dqs(HPS_DDR3_DQS_P),                             //                               .mem_dqs
               .memory_mem_dqs_n(HPS_DDR3_DQS_N),                           //                               .mem_dqs_n
               .memory_mem_odt(HPS_DDR3_ODT),                               //                               .mem_odt
               .memory_mem_dm(HPS_DDR3_DM),                                 //                               .mem_dm
               .memory_oct_rzqin(HPS_DDR3_RZQ),                             //                               .oct_rzqin
               //HPS ethernet
               .hps_0_hps_io_hps_io_emac1_inst_TX_CLK(HPS_ENET_GTX_CLK),    //                   hps_0_hps_io.hps_io_emac1_inst_TX_CLK
               .hps_0_hps_io_hps_io_emac1_inst_TXD0(HPS_ENET_TX_DATA[0]),   //                               .hps_io_emac1_inst_TXD0
               .hps_0_hps_io_hps_io_emac1_inst_TXD1(HPS_ENET_TX_DATA[1]),   //                               .hps_io_emac1_inst_TXD1
               .hps_0_hps_io_hps_io_emac1_inst_TXD2(HPS_ENET_TX_DATA[2]),   //                               .hps_io_emac1_inst_TXD2
               .hps_0_hps_io_hps_io_emac1_inst_TXD3(HPS_ENET_TX_DATA[3]),   //                               .hps_io_emac1_inst_TXD3
               .hps_0_hps_io_hps_io_emac1_inst_RXD0(HPS_ENET_RX_DATA[0]),   //                               .hps_io_emac1_inst_RXD0
               .hps_0_hps_io_hps_io_emac1_inst_MDIO(HPS_ENET_MDIO),         //                               .hps_io_emac1_inst_MDIO
               .hps_0_hps_io_hps_io_emac1_inst_MDC(HPS_ENET_MDC),           //                               .hps_io_emac1_inst_MDC
               .hps_0_hps_io_hps_io_emac1_inst_RX_CTL(HPS_ENET_RX_DV),      //                               .hps_io_emac1_inst_RX_CTL
               .hps_0_hps_io_hps_io_emac1_inst_TX_CTL(HPS_ENET_TX_EN),      //                               .hps_io_emac1_inst_TX_CTL
               .hps_0_hps_io_hps_io_emac1_inst_RX_CLK(HPS_ENET_RX_CLK),     //                               .hps_io_emac1_inst_RX_CLK
               .hps_0_hps_io_hps_io_emac1_inst_RXD1(HPS_ENET_RX_DATA[1]),   //                               .hps_io_emac1_inst_RXD1
               .hps_0_hps_io_hps_io_emac1_inst_RXD2(HPS_ENET_RX_DATA[2]),   //                               .hps_io_emac1_inst_RXD2
               .hps_0_hps_io_hps_io_emac1_inst_RXD3(HPS_ENET_RX_DATA[3]),   //                               .hps_io_emac1_inst_RXD3
               //HPS SD card
               .hps_0_hps_io_hps_io_sdio_inst_CMD(HPS_SD_CMD),              //                               .hps_io_sdio_inst_CMD
               .hps_0_hps_io_hps_io_sdio_inst_D0(HPS_SD_DATA[0]),           //                               .hps_io_sdio_inst_D0
               .hps_0_hps_io_hps_io_sdio_inst_D1(HPS_SD_DATA[1]),           //                               .hps_io_sdio_inst_D1
               .hps_0_hps_io_hps_io_sdio_inst_CLK(HPS_SD_CLK),              //                               .hps_io_sdio_inst_CLK
               .hps_0_hps_io_hps_io_sdio_inst_D2(HPS_SD_DATA[2]),           //                               .hps_io_sdio_inst_D2
               .hps_0_hps_io_hps_io_sdio_inst_D3(HPS_SD_DATA[3]),           //                               .hps_io_sdio_inst_D3
               //HPS USB
               .hps_0_hps_io_hps_io_usb1_inst_D0(HPS_USB_DATA[0]),          //                               .hps_io_usb1_inst_D0
               .hps_0_hps_io_hps_io_usb1_inst_D1(HPS_USB_DATA[1]),          //                               .hps_io_usb1_inst_D1
               .hps_0_hps_io_hps_io_usb1_inst_D2(HPS_USB_DATA[2]),          //                               .hps_io_usb1_inst_D2
               .hps_0_hps_io_hps_io_usb1_inst_D3(HPS_USB_DATA[3]),          //                               .hps_io_usb1_inst_D3
               .hps_0_hps_io_hps_io_usb1_inst_D4(HPS_USB_DATA[4]),          //                               .hps_io_usb1_inst_D4
               .hps_0_hps_io_hps_io_usb1_inst_D5(HPS_USB_DATA[5]),          //                               .hps_io_usb1_inst_D5
               .hps_0_hps_io_hps_io_usb1_inst_D6(HPS_USB_DATA[6]),          //                               .hps_io_usb1_inst_D6
               .hps_0_hps_io_hps_io_usb1_inst_D7(HPS_USB_DATA[7]),          //                               .hps_io_usb1_inst_D7
               .hps_0_hps_io_hps_io_usb1_inst_CLK(HPS_USB_CLKOUT),          //                               .hps_io_usb1_inst_CLK
               .hps_0_hps_io_hps_io_usb1_inst_STP(HPS_USB_STP),             //                               .hps_io_usb1_inst_STP
               .hps_0_hps_io_hps_io_usb1_inst_DIR(HPS_USB_DIR),             //                               .hps_io_usb1_inst_DIR
               .hps_0_hps_io_hps_io_usb1_inst_NXT(HPS_USB_NXT),             //                               .hps_io_usb1_inst_NXT
               //HPS SPI
               .hps_0_hps_io_hps_io_spim1_inst_CLK(HPS_SPIM_CLK),           //                               .hps_io_spim1_inst_CLK
               .hps_0_hps_io_hps_io_spim1_inst_MOSI(HPS_SPIM_MOSI),         //                               .hps_io_spim1_inst_MOSI
               .hps_0_hps_io_hps_io_spim1_inst_MISO(HPS_SPIM_MISO),         //                               .hps_io_spim1_inst_MISO
               .hps_0_hps_io_hps_io_spim1_inst_SS0(HPS_SPIM_SS),            //                               .hps_io_spim1_inst_SS0
               //HPS UART
               .hps_0_hps_io_hps_io_uart0_inst_RX(HPS_UART_RX),             //                               .hps_io_uart0_inst_RX
               .hps_0_hps_io_hps_io_uart0_inst_TX(HPS_UART_TX),             //                               .hps_io_uart0_inst_TX
               //HPS I2C1
               .hps_0_hps_io_hps_io_i2c0_inst_SDA(HPS_I2C0_SDAT),           //                               .hps_io_i2c0_inst_SDA
               .hps_0_hps_io_hps_io_i2c0_inst_SCL(HPS_I2C0_SCLK),           //                               .hps_io_i2c0_inst_SCL
               //HPS I2C2
               .hps_0_hps_io_hps_io_i2c1_inst_SDA(HPS_I2C1_SDAT),           //                               .hps_io_i2c1_inst_SDA
               .hps_0_hps_io_hps_io_i2c1_inst_SCL(HPS_I2C1_SCLK),           //                               .hps_io_i2c1_inst_SCL
               //GPIO
               .hps_0_hps_io_hps_io_gpio_inst_GPIO09(HPS_CONV_USB_N),       //                               .hps_io_gpio_inst_GPIO09
               .hps_0_hps_io_hps_io_gpio_inst_GPIO35(HPS_ENET_INT_N),       //                               .hps_io_gpio_inst_GPIO35
               .hps_0_hps_io_hps_io_gpio_inst_GPIO40(HPS_LTC_GPIO),         //                               .hps_io_gpio_inst_GPIO40
               .hps_0_hps_io_hps_io_gpio_inst_GPIO53(HPS_LED),              //                               .hps_io_gpio_inst_GPIO53
               .hps_0_hps_io_hps_io_gpio_inst_GPIO54(HPS_KEY),              //                               .hps_io_gpio_inst_GPIO54
               .hps_0_hps_io_hps_io_gpio_inst_GPIO61(HPS_GSENSOR_INT),      //                               .hps_io_gpio_inst_GPIO61
               //FPGA Partion
               .led_pio_external_connection_export         (fpga_led_internal),      //    led_pio_external_connection.export
               .dipsw_pio_external_connection_export       (SW),                   //  dipsw_pio_external_connection.export               
                    .button_pio_external_connection_export      (fpga_debounced_buttons),// button_pio_external_connection.export
                    // My signals
                .pio_fsm_cmnd_wr_external_connection_export   (i_fsm_cmnd_wr),
                .pio_fsm_add_external_connection_export       (i_fsm_add),       //       pio_fsm_add_external_connection.export
                .pio_fsm_aux1n_external_connection_export     (o_fsm_aux1n),     //     pio_fsm_aux1n_external_connection.export
                .pio_fsm_aux2n_external_connection_export     (o_fsm_aux2n),     //     pio_fsm_aux2n_external_connection.export
                .pio_fsm_busy_external_connection_export      (o_fsm_busy),      //      pio_fsm_busy_external_connection.export
                .pio_fsm_cmnd_ctrl_external_connection_export (i_fsm_cmnd_ctrl), // pio_fsm_cmnd_ctrl_external_connection.export
                .pio_fsm_cmnd_rd_external_connection_export   (o_fsm_cmnd_rd),   //   pio_fsm_cmnd_rd_external_connection.export
                .pio_fsm_data_rd_external_connection_export   (o_fsm_data_rd),   //   pio_fsm_data_rd_external_connection.export
                .pio_fsm_data_wr_external_connection_export   (i_fsm_data_wr),   //   pio_fsm_data_wr_external_connection.export
                .pio_fsm_wr_en_external_connection_export     (i_fsm_wr_en),     //     pio_fsm_wr_en_external_connection.export
        
                    
                    .sram_address                              (sram_address_sig),                         
                    .sram_chipselect                           (sram_chipselect_sig),                       
                    .sram_clken                                (sram_clken_sig),                        
                    .sram_write                                (sram_write_sig),                     
                    .sram_readdata                             (sram_readdata_sig),                       
                    .sram_writedata                            (sram_writedata_sig),                      
                    .sram_byteenable                           (sram_byteenable_sig),   
                    
               .hps_0_h2f_reset_reset_n(hps_fpga_reset_n),                  //                hps_0_h2f_reset.reset_n
               .hps_0_f2h_cold_reset_req_reset_n(~hps_cold_reset),          //       hps_0_f2h_cold_reset_req.reset_n
               .hps_0_f2h_debug_reset_req_reset_n(~hps_debug_reset),        //      hps_0_f2h_debug_reset_req.reset_n
               .hps_0_f2h_stm_hw_events_stm_hwevents(stm_hw_events),        //        hps_0_f2h_stm_hw_events.stm_hwevents
               .hps_0_f2h_warm_reset_req_reset_n(~hps_warm_reset),          //       hps_0_f2h_warm_reset_req.reset_n

           );

// Debounce logic to clean out glitches within 1ms
debounce debounce_inst(
             .clk(fpga_clk_50),
             .reset_n(hps_fpga_reset_n),
             .data_in(KEY),
             .data_out(fpga_debounced_buttons)
         );
defparam debounce_inst.WIDTH = 2;
defparam debounce_inst.POLARITY = "LOW";
defparam debounce_inst.TIMEOUT = 50000;               // at 50Mhz this is a debounce time of 1ms
defparam debounce_inst.TIMEOUT_WIDTH = 16;            // ceil(log2(TIMEOUT))

// Source/Probe megawizard instance
hps_reset hps_reset_inst(
              .source_clk(fpga_clk_50),
              .source(hps_reset_req)
          );

altera_edge_detector pulse_cold_reset(
                         .clk(fpga_clk_50),
                         .rst_n(hps_fpga_reset_n),
                         .signal_in(hps_reset_req[0]),
                         .pulse_out(hps_cold_reset)
                     );
defparam pulse_cold_reset.PULSE_EXT = 6;
defparam pulse_cold_reset.EDGE_TYPE = 1;
defparam pulse_cold_reset.IGNORE_RST_WHILE_BUSY = 1;

altera_edge_detector pulse_warm_reset(
                         .clk(fpga_clk_50),
                         .rst_n(hps_fpga_reset_n),
                         .signal_in(hps_reset_req[1]),
                         .pulse_out(hps_warm_reset)
                     );
defparam pulse_warm_reset.PULSE_EXT = 2;
defparam pulse_warm_reset.EDGE_TYPE = 1;
defparam pulse_warm_reset.IGNORE_RST_WHILE_BUSY = 1;

altera_edge_detector pulse_debug_reset(
                         .clk(fpga_clk_50),
                         .rst_n(hps_fpga_reset_n),
                         .signal_in(hps_reset_req[2]),
                         .pulse_out(hps_debug_reset)
                     );
defparam pulse_debug_reset.PULSE_EXT = 32;
defparam pulse_debug_reset.EDGE_TYPE = 1;
defparam pulse_debug_reset.IGNORE_RST_WHILE_BUSY = 1;

reg [25: 0] counter;
reg led_level;
always @(posedge fpga_clk_50 or negedge hps_fpga_reset_n) begin
    if (~hps_fpga_reset_n) begin
        counter <= 0;
        led_level <= 0;
    end

    else if (counter == 24999999) begin
        counter <= 0;
        led_level <= ~led_level;
    end
    else
        counter <= counter + 1'b1;
end

assign LED[0] = led_level;
/**
 * Chip Connection
 *
    output              chip_clk,
    output   [ 7: 0]    chip_i_data,
    output              chip_i_wren,
    output              chip_i_rst,
    output              chip_i_en,
    output   [ 3: 0]    chip_i_add,
    input    [ 7: 0]    chip_o_result,
    input               chip_o_done 
 *
 **/
// Chip Input
assign chip_i_data = i_fsm_data_wr[7:0]; 
assign chip_i_wren = i_fsm_wr_en;
assign chip_i_rst = i_fsm_cmnd_wr[0];
assign chip_i_en  = i_fsm_cmnd_wr[1];
assign chip_i_add = i_fsm_add[3:0];
// Chip Output
assign o_fsm_data_rd[7:0] = chip_o_result;
assign o_fsm_busy = o_done_reg;

//-----------------------------------------------------------------
// o_done Registered
//-----------------------------------------------------------------
assign rst_o_done = i_fsm_cmnd_wr[2];
reg o_done_reg;

always @(posedge fpga_clk_50 or posedge rst_o_done) begin
    if (rst_o_done) begin
        o_done_reg <= 1'b0;  
    end else if (chip_o_done) begin
        o_done_reg <= 1'b1;  
    end
end

//-----------------------------------------------------------------
// Vamos a utilizar el i_fsm_cmnd_ctrl para manejar el reloj
// Que irá conectado a la FPGA
//-----------------------------------------------------------------
reg [16:0] contador;

// Contador de 17 bits
always @(posedge fpga_clk_50) begin
    if (contador == 17'h1FFFF) // Reiniciar contador 
        contador <= 0;
    else
        contador <= contador + 1;
end

reg chip_clk_var;

// Multiplexor para seleccionar bits del contador
always @* begin
    case (i_fsm_cmnd_ctrl)
        8'b00000000: chip_clk_var = contador[0];   // 25 MHz
        8'b00000001: chip_clk_var = contador[1];   // 12.5 MHz
        8'b00000010: chip_clk_var = contador[2];   // 6.25 MHz
        8'b00000011: chip_clk_var = contador[3];   // 3.125 MHz
        8'b00000100: chip_clk_var = contador[4];   // 1.5625 MHz
        8'b00000101: chip_clk_var = contador[5];   // 781.25 kHz
        8'b00000110: chip_clk_var = contador[6];   // 390.625 kHz
        8'b00000111: chip_clk_var = contador[7];   // 195.3125 kHz
        8'b00001000: chip_clk_var = contador[8];   // 97.65625 kHz
        8'b00001001: chip_clk_var = contador[9];   // 48.828125 kHz
        8'b00001010: chip_clk_var = contador[10];  // 24.4140625 kHz
        8'b00001011: chip_clk_var = contador[11];  // 12.20703125 kHz
        8'b00001100: chip_clk_var = contador[12];  // 6.103515625 kHz
        8'b00001101: chip_clk_var = contador[13];  // 3.0517578125 kHz
        8'b00001110: chip_clk_var = contador[14];  // 1.52587890625 kHz
        8'b00001111: chip_clk_var = contador[15];  // 762.939453125 Hz
        8'b00010000: chip_clk_var = contador[16];  // 381.4697265625 Hz
        default: chip_clk_var = 1'b0; // Valor por defecto
    endcase
end


assign chip_clk = chip_clk_var;



endmodule
