/**
 **********************************************************************************************
 * Project Name: Test chip 32n
 * 		
 * @brief: Este proyecto se ha creado para testear el chip de 32 neuronas. 
 * 
 * @note to run : gcc -Wall -Wextra -o main main.c src/*.c
 * 
 * ********************************************************************************************
 */

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <sys/time.h> 
#include <math.h> 

#include "src/platform_header.h"
#include "src/log.h"
#include "src/chip32n.h"
#include "src/init_file.h"
#include "src/misc.h"
#include "src/my_timer.h"

/*********************************************
* version		Fecha		Descripcion
* ----------------------------------------
* 1.0			30/01/2024	Inicial
* 1.2			09/07/2024	Velocidad de reloj mas baja para chip
* *******************************************/
#define APP_VERSION	"ARM App version 1.2, Test Chip 32n"

#define IMG_LEN     784
#define OUT_LEN     10
/**************************************************************************************************
*
* TYPE DEFINITION
*
*
*
*
* *************************************************************************************************/


/**************************************************************************************************
*
* Functions
*
*
*
*
* *************************************************************************************************/
bool chip_inference_img(const int8_t * const img, const size_t img_len, uint16_t * o_result)
{

    chip32n__reset();
    
    // LOGLN("chip32n__send_img...");
    chip32n__send_img(img, img_len);    
    
    chip32n__reset(); // according to testbench, we must reset again the chip
    
    // LOGLN("chip32n__process(1000)...");
    bool flag = chip32n__process(1000);
    if(flag) 
    {
        // LOGLN("chip32n__process OK");
    }
    else 
    {
        LOGLN("chip32n__process ERROR: signal 'o_done' not detected !");
        return false;
    }

    // LOGLN("chip32n__read_output()...");
    chip32n__read_output(o_result);
    return true;
}

void buffer_to_csv(FILE * file_csv, const uint16_t * buffer, const uint16_t buf_len)
{
    size_t i;
    for (i = 0; i < (buf_len-1); i++)
    {
        fprintf(file_csv, "%d,", buffer[i]);
    }
    fprintf(file_csv, "%d\n", buffer[i]);    
}
/**************************************************************************************************
*
*
*
*
*
*
*
*
*
*
*
*
*
*
*
*
*
*
* *************************************************************************************************/
int main() {
    
    platform_init();
    
    LOG("______________________________________________\n\n"); 
    LOG(APP_VERSION "\n");
    LOG("______________________________________________\n\n"); 

    init_file_t init_file;
    init_file__init(&init_file);
    init_file__print(&init_file);
    // inicializa chip
    chip32n_cfg_t chip32n_cfg;
    chip32n_cfg.chip_clk_freq = init_file.freq_clk;
    chip32n__init(&chip32n_cfg);

    
    chip32n__reset();

#if 0 // test de envio de imagen
    int8_t img[256];
    for (size_t i = 0; i < sizeof(img); i++)
    {
        img[i] = (int8_t)i;
    }
    chip32n__send_img(img, sizeof(img));
#endif

#if 0 // test de espera de o_done
    LOGLN("Test de espera o_done='1' por 10 segundos...");
    LOGLN("chip32n__process(10000)...");
    bool flag = chip32n__process(10000);
    if(flag) {LOGLN("chip32n__process OK");}
    else {LOGLN("chip32n__process ERROR");}
#endif

#if 0 // lectura de salida resultados
    LOGLN("");
    LOGLN("chip32n__read_output()...");
    int8_t o_buffer[10];
    chip32n__read_output(o_buffer);
#endif

#if 1 // inference the whole test set
    int8_t img[IMG_LEN];
    uint16_t o_result[OUT_LEN];
    uint8_t o_result_gt;
    uint16_t i,j;
    bool flag;
    uint8_t bit_res = 8;
    int cont_score_sw_cmp = 0;
    struct timeval t1, t2, t3, t4, t5;
    float score = 0;
    mytime_t timer1;

    LOGLN("-----------------------------------");
    LOGLN("Whole Test set evaluation:");
    LOGLN("-----------------------------------");
    LOGLN("Input Test set file read : %s", init_file.test_set_path);
    LOGLN("Output Test set file read : %s", init_file.test_set_y_ctg_path);
    LOGLN("Num samples = %u", init_file.n_samples);
    LOGLN("-----------------------------------");
    LOGLN("inference...");
    LOGLN("-----------------------------------");

    FILE * x_file = fopen(init_file.test_set_path, "r");
    FILE * y_file = fopen(init_file.test_set_y_ctg_path, "r");
    FILE * out_file = fopen("Output/output.csv", "w");

    // start timer
	gettimeofday(&t1, NULL);

    for (i=0 ; i<init_file.n_samples ; i++)
    {
        //---------------------------------
        // leo imagen 
        //---------------------------------	
        for (j=0 ; j<IMG_LEN ; j++) 
        {
            fscanf(x_file, "%hd\n", (short int *)&img[j]);
        }
        flag = chip_inference_img(img, sizeof(img), o_result);       
        if (!flag){
            LOGLN("Error in inference aborting.");
            break;
        } 
        buffer_to_csv(out_file, (const uint16_t *)o_result, OUT_LEN);
        
        //---------------------------------
        // compute in real time the score
        //---------------------------------
        int8_t one_hot_buffer[OUT_LEN];
        uint16_t o_result_one_hot = neuron_out_to_one_hot(o_result, OUT_LEN, bit_res, one_hot_buffer);
    // LOGLN("o_result_one_hot = %d", o_result_one_hot);
        int y_chip = one_hot_bin_to_catg_int(o_result_one_hot, OUT_LEN);		
    // LOGLN("y_chip = %d", y_chip);
        // read the ground truth
        fscanf(y_file, "%hd\n", &o_result_gt);            
        
    // LOGLN("o_result_gt = %u", o_result_gt);
    
        // calculates score
        if (y_chip == o_result_gt)
            cont_score_sw_cmp++;
        // print the score
        score = (float)(cont_score_sw_cmp)*100 / (float)(i + 1);

        // Calculate time lapsed
        gettimeofday(&t2, NULL);
        time_lapsed(&timer1, &t1, &t2);
        // print results for this sample
		printf("\rn = %d  Score = %.2f%%    %d s        ", i, score, timer1.segundos);        
		// printf("n = %d  Score = %.2f%%    %d s        \r\n", i, score, timer1.segundos);        

    }
    fclose(x_file);
    fclose(y_file);
    fclose(out_file);
#endif

    platform_end();
    return (0);
}


