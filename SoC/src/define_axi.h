/*************/
/* FROM QSYS */
/*************/

#define ALT_LWFPGASLVS_OFST 0xff200000      // GENERAL
#define HW_REGS_BASE 0xfc000000
#define HW_REGS_SPAN ( 0x04000000 )
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )

// get pointer to hardware register connected to light AXI bridge
#define GET_POINTER_TO_LIGHT_AXI(virtual_base, REG_BASE) (unsigned int *)( virtual_base + ( ( unsigned long  )( ALT_LWFPGASLVS_OFST + REG_BASE ) & ( unsigned long)( HW_REGS_MASK ) ) );
