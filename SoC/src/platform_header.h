/*

 Para ARM system SoC

*/
#ifndef PLATFORM_H_
#define PLATFORM_H_

#include <inttypes.h>
#include <stdbool.h>

void        platform_init(void);

void        platform__gpio_set_clk_freq(uint8_t value);
void        platform__gpio_wr_data(uint8_t value);
void        platform__gpio_wr_wren(uint8_t value);
void        platform__gpio_wr_add(uint8_t value);
void        platform__gpio_wr_ctrl(uint8_t value);

uint32_t    platform__gpio_rd_data_result(void);
bool        platform__gpio_rd_o_done(void);

void        platform_end(void);

#endif
