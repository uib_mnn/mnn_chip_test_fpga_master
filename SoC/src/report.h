
/*
Como usar:
    - Declarar variables globales:

    - para realizar reportes:
        REPORT(g_reportStr, <mensaje>); REPORT_NOW();

*/


// test para evitar bug de no escribe en .txt output
#define REPORT		sprintf
//#define REPORT		

// PRINTF DEFINES
#define REPORT_BULLET		"> "
// #define REPORT_ERROR			PRINTF_ANSI_COLOR_RED  "[ERROR]\t" PRINTF_SYSTEM_COLOR
// #define REPORT_OK			PRINTF_ANSI_COLOR_GREEN "[ OK ]\t" PRINTF_SYSTEM_COLOR
#define REPORT_ERROR		"[ERROR] " 
#define REPORT_OK			"[ok] " 
#define REPORT_TAB			"      "
#define REPORT_INTRO()		REPORT(g_reportStr, "\n"); REPORT_NOW()

// report definition
// #define REPORT_NOW()	printf(g_reportStr); fprintf(file_report, g_reportStr)
#define REPORT_NOW()	printf(g_reportStr)
