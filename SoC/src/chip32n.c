/*******************************************************************************************
* INCLUDES
********************************************************************************************/
#include "chip32n.h"

#include <stdint.h>
#include <unistd.h>
#include <time.h>

#include "log.h"
#include "platform_header.h"

/*******************************************************************************************
* PRIVATE MACROS
********************************************************************************************/
#define FILE_NAME_INIT      	"Input/init.txt"
/********************************************************************************************
* PRIVATE TYPES
********************************************************************************************/
/**
 * Chip Gpio management
*/
// write
#define CHIP_CLK_FREQ(x)        platform__gpio_set_clk_freq(x)
#define CHIP_WR_DATA(x)         platform__gpio_wr_data(x)
#define CHIP_WR_WREN(x)         platform__gpio_wr_wren(x)
#define CHIP_WR_ADD_RESULT(x)          platform__gpio_wr_add(x)
// read
#define CHIP_RD_DATA_RESULT()           platform__gpio_rd_data_result()
#define CHIP_RD_O_DONE()                platform__gpio_rd_o_done()
/**
 * @brief:
 * manejo de pines de control. 
*/
#define GPIO_CTRL_PIN_RST		    0b00000001
#define GPIO_CTRL_PIN_EN 		    0b00000010
#define GPIO_CTRL_PIN_CLRODONE      0b00000100

#define CHIP_RST_HIGH()			gpio_ctrl |=  GPIO_CTRL_PIN_RST; platform__gpio_wr_ctrl(gpio_ctrl)
#define CHIP_RST_LOW()			gpio_ctrl &= ~GPIO_CTRL_PIN_RST; platform__gpio_wr_ctrl(gpio_ctrl)

#define CHIP_EN_HIGH()			gpio_ctrl |=  GPIO_CTRL_PIN_EN; platform__gpio_wr_ctrl(gpio_ctrl)
#define CHIP_EN_LOW()			gpio_ctrl &= ~GPIO_CTRL_PIN_EN; platform__gpio_wr_ctrl(gpio_ctrl)

#define CHIP_CLRODONE_HIGH()	gpio_ctrl |=  GPIO_CTRL_PIN_CLRODONE; platform__gpio_wr_ctrl(gpio_ctrl)
#define CHIP_CLRODONE_LOW()		gpio_ctrl &= ~GPIO_CTRL_PIN_CLRODONE; platform__gpio_wr_ctrl(gpio_ctrl)

/********************************************************************************************
* PRIVATE VARIABLES
********************************************************************************************/
static chip32n_cfg_t chip32n_cfg;
static uint8_t gpio_ctrl = 0;

const char * freq_clk_str[] = {
    "25.0 MHz",   // 25 MHz
    "12.5 MHz",   // 12.5 MHz
    "6.3 MHz",    // 6.25 MHz
    "3.1 MHz",    // 3.125 MHz
    "1.6 MHz",    // 1.5625 MHz
    "0.8 MHz",    // 781.25 kHz
    "0.4 MHz",    // 390.625 kHz
    "0.2 MHz",    // 195.3125 kHz
    "0.1 MHz",    // 97.65625 kHz
    "48.8 kHz",   // 48.828125 kHz
    "24.4 kHz",   // 24.4140625 kHz
    "12.2 kHz",   // 12.20703125 kHz
    "6.1 kHz",    // 6.103515625 kHz
    "3.1 kHz",    // 3.0517578125 kHz
    "1.5 kHz",    // 1.52587890625 kHz
    "762.9 Hz",   // 762.939453125 Hz
    "381.5 Hz"    // 381.4697265625 Hz
};

/**
 * @brief tabla para saber 1 ciclo de reloj del chip a cuantos
 * microsegundos corresponde
 * 
 */
const uint8_t clk_2_useg[CHIP_CLK_FREQ_LEN] = {
    1,   // "25 MHz" -> 0.04 us -> 1 (rounded)
    1,   // "12.5 MHz" -> 0.08 us -> 1 (rounded)
    1,   // "6.25 MHz" -> 0.16 us -> 1 (rounded)
    1,   // "3.125 MHz" -> 0.32 us -> 1 (rounded)
    1,   // "1.5625 MHz" -> 0.64 us -> 1 (rounded)
    2,   // "781.25 kHz" -> 1.28 us -> 2
    3,   // "390.625 kHz" -> 2.56 us -> 3 (rounded)
    6,   // "195.3125 kHz" -> 5.12 us -> 6 (rounded)
    11,  // "97.65625 kHz" -> 10.24 us -> 11 (rounded)
    21,  // "48.828125 kHz" -> 20.48 us -> 21 (rounded)
    41,  // "24.4140625 kHz" -> 40.96 us -> 41 (rounded)
    82,  // "12.20703125 kHz" -> 81.92 us -> 82 (rounded)
    164, // "6.103515625 kHz" -> 163.84 us -> 164 (rounded)
    328, // "3.0517578125 kHz" -> 327.68 us -> 328 (rounded)
    656, // "1.52587890625 kHz" -> 655.36 us -> 656 (rounded)
    1313,// "762.939453125 Hz" -> 1310.72 us -> 1313 (rounded)
    2627 // "381.4697265625 Hz" -> 2621.44 us -> 2627 (rounded)
};

/********************************************************************************************
* PUBLIC VARIABLES
********************************************************************************************/

/********************************************************************************************
* PRIVATE FUNCTIONS
********************************************************************************************/

static void chip_delay_clk_cycles(uint16_t clk_cycles)
{
    uint16_t us = clk_2_useg[chip32n_cfg.chip_clk_freq]*clk_cycles;
    usleep(us);
}
/********************************************************************************************
* PUBLIC FUNCTIONS
********************************************************************************************/

void chip32n__init(chip32n_cfg_t * cfg)
{
    chip32n_cfg = *cfg;
    gpio_ctrl = 0;

    LOG("Init Chip"LOG_EOL);
    LOG("---------------------"LOG_EOL);
    LOG(LOG_TAB "Setting chip frequency : %s" LOG_EOL, chip32n__get_freq_clk_str(cfg->chip_clk_freq));
    CHIP_CLK_FREQ((uint8_t)cfg->chip_clk_freq);
    // Set pins
    CHIP_RST_HIGH();
    CHIP_EN_LOW();
    CHIP_WR_DATA(0);
    CHIP_WR_WREN(0);
    CHIP_WR_ADD_RESULT(0);
    LOG(LOG_TAB "Setting Pinout for Chip : " LOG_EOL);
    LOG(LOG_TAB LOG_TAB "chip_rst = 1" LOG_EOL);
    LOG(LOG_TAB LOG_TAB "chip_en = 0" LOG_EOL);
    LOG(LOG_TAB LOG_TAB "chip_data = 0" LOG_EOL);
    LOG(LOG_TAB LOG_TAB "chip_wr_en = 0" LOG_EOL);
    LOG(LOG_TAB LOG_TAB "chip_add = 0" LOG_EOL);
}

const char * chip32n__get_freq_clk_str(chip_clk_freq_t freq_clk)
{
    return freq_clk_str[freq_clk];
}

void chip32n__reset(void)
{
    // LOGLN("chip32n__reset()...");
    CHIP_RST_HIGH();
    chip_delay_clk_cycles(16);
    CHIP_RST_LOW();
    chip_delay_clk_cycles(8);
}

void chip32n__send_img(const int8_t * const img, const size_t img_len)
{
    size_t i;
    for (i = 0; i < img_len; i++)
    {
        CHIP_WR_DATA(img[i]);
        chip_delay_clk_cycles(2);
        CHIP_WR_WREN(1);
        chip_delay_clk_cycles(2);
        CHIP_WR_WREN(0);
        chip_delay_clk_cycles(2);
    }    
}

void chip32n__clr_o_done_reg(void)
{
    CHIP_CLRODONE_HIGH();
    CHIP_CLRODONE_LOW();
}

bool chip32n__process(uint16_t timeout_ms)
{    
    chip32n__clr_o_done_reg();
    CHIP_EN_HIGH();
    clock_t start_time = clock();
    while (!CHIP_RD_O_DONE()) 
    {
        // Verificar si ha pasado el tiempo de espera
        clock_t current_time = clock();
        double elapsed_time = ((double)(current_time - start_time)) / CLOCKS_PER_SEC * 1000; // Tiempo transcurrido en milisegundos
    
        if (elapsed_time >= timeout_ms) {
            // Se ha alcanzado el tiempo de espera
            // CHIP_EN_LOW();
            return false;
        }
    }
    // CHIP_EN_LOW(); // todo: this should work but it doesnt, have a look why not
    return true;
}

void chip32n__read_output(uint16_t * o_buffer)
{
    size_t i;
    for (i = 0; i < 10; i++)
    {
        CHIP_WR_ADD_RESULT(i);        
        chip_delay_clk_cycles(2);
        o_buffer[i] = (uint16_t)CHIP_RD_DATA_RESULT();
        chip_delay_clk_cycles(2);
    }  
}

