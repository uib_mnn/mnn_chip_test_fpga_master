/** -----------------------------------------------------------------------
 * 
 * @brief      
 * 
 * 
 * @dependencies
 * 
 * 
 * @example
 * 
 * 
 * @Historic:
 *    ----------------------------------------------------------------------- 
 *    Version      Authors         Date                Changes
 *    ----------------------------------------------------------------------- 
 *    0.1          CCFF            xx/xx/xxxx          Initial
 * 
 * -------------------------------------------------------------------------*/
#ifndef CHIP32N_H_
#define CHIP32N_H_
/*******************************************************************************************
* INCLUDES
********************************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
/*******************************************************************************************
* PUBLIC MACROS
********************************************************************************************/
/********************************************************************************************
* PUBLIC TYPES
********************************************************************************************/
typedef enum {
    CHIP_CLK_FREQ_0,  // CLK_FREQ = 25 MHz
    CHIP_CLK_FREQ_1,  // CLK_FREQ = 12.5 MHz
    CHIP_CLK_FREQ_2,  // CLK_FREQ = 6.25 MHz
    CHIP_CLK_FREQ_3,  // CLK_FREQ = 3.125 MHz
    CHIP_CLK_FREQ_4,  // CLK_FREQ = 1.5625 MHz
    CHIP_CLK_FREQ_5,  // CLK_FREQ = 781.25 kHz
    CHIP_CLK_FREQ_6,  // CLK_FREQ = 390.625 kHz
    CHIP_CLK_FREQ_7,  // CLK_FREQ = 195.3125 kHz
    CHIP_CLK_FREQ_8,  // CLK_FREQ = 97.65625 kHz
    CHIP_CLK_FREQ_9,  // CLK_FREQ = 48.828125 kHz
    CHIP_CLK_FREQ_10, // CLK_FREQ = 24.4140625 kHz
    CHIP_CLK_FREQ_11, // CLK_FREQ = 12.20703125 kHz
    CHIP_CLK_FREQ_12, // CLK_FREQ = 6.103515625 kHz
    CHIP_CLK_FREQ_13, // CLK_FREQ = 3.0517578125 kHz
    CHIP_CLK_FREQ_14, // CLK_FREQ = 1.52587890625 kHz
    CHIP_CLK_FREQ_15, // CLK_FREQ = 762.939453125 Hz
    CHIP_CLK_FREQ_16, // CLK_FREQ = 381.4697265625 Hz
    CHIP_CLK_FREQ_LEN
} chip_clk_freq_t;



typedef struct {
    chip_clk_freq_t chip_clk_freq;
} chip32n_cfg_t;
/********************************************************************************************
* PUBLIC VARIABLES
********************************************************************************************/

/********************************************************************************************
* PUBLIC PROTOTYPES
********************************************************************************************/
void            chip32n__init               (chip32n_cfg_t * cfg);
const char *    chip32n__get_freq_clk_str   (chip_clk_freq_t freq_clk);
void            chip32n__reset              (void);
void            chip32n__send_img           (const int8_t * const img, const size_t img_len);
void            chip32n__clr_o_done_reg     (void);
bool            chip32n__process            (uint16_t timeout_ms);
void            chip32n__read_output        (uint16_t * o_buffer);

#endif


