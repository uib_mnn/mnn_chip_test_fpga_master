/*---------------------------------------------------------------


how to use:

- declare variables
    // SYSTEM TIMER
	crono_t crono1;
	struct timeval t1,t2; 
	mytime_t timer1;
	float time1sample;
	float time_lapsed_sec;	

- initialize
	gettimeofday(&t1, NULL);
	//strcpy(crono2.c_str, "xx:xx:xx");

- Read time lapsed
    // time lapsed
	gettimeofday(&t2, NULL); 
	time_lapsed(&timer1, &t1, &t2)
    time_lapsed_sec = timer1.segundos; // lee los segundos
    seconds_2_crono(time_lapsed_sec, &crono1); // guarda en struct crono en formato fecha
    printf("%-18s", crono1.c_str); 

-------------------------------------------------------------------*/
typedef struct {
	int segundos;
	int msegundos;
} mytime_t;

typedef struct {
	int hour;
	int min;
	int sec;
	char c_str[9];
} crono_t;

/*-----------------------------------------------------------------------
@Description: Timer funcions

@Arguments:

@returns:

@Template:

-------------------------------------------------------------------------*/
void time_lapsed(mytime_t *myclock, struct timeval *t1, struct timeval *t2){
	//long tmp;
	myclock->segundos = t2->tv_sec - t1->tv_sec;
	//tmp = (t2->tv_sec - t1->tv_sec)*1000;
	myclock->msegundos = (t2->tv_sec - t1->tv_sec)*1000 + (t2->tv_usec - t1->tv_usec)/1000 ;
}
void seconds_2_crono(int seconds, crono_t *crono){

	crono->hour = seconds/3600;
	crono->min = (seconds - crono->hour*3600) / 60;
	crono->sec = seconds - crono->hour*3600 - crono->min*60;
	
	sprintf(crono->c_str, "%02u:%02u:%02u",crono->hour,crono->min,crono->sec);
}



