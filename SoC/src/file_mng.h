
/*-----------------------------------------------------------------------
@Description:
// count number of lines in a text file
@Arguments:

@returns:

@Template:

-------------------------------------------------------------------------*/

int countlines(char *myfilename){
	FILE *myfile;
	int ch = 0, numlines = 0;;
	myfile = fopen(myfilename, "r");
	if (myfile == NULL){
		printf("Error reading %s\n", myfilename); 
		return(0);

	}
	while ((ch = fgetc(myfile)) != EOF){
		if (ch == '\n')
			numlines++;
	}
	return numlines;
}
/*-----------------------------------------------------------------------
@Description:
// count number of cols in a text file
@Arguments:

@returns:

@Template:

-------------------------------------------------------------------------*/
int countColumns(char *myfilename){
	FILE *myfile;
	int ch = 0;
	int numCol = 0;

	myfile = fopen(myfilename, "r");
	if (myfile == NULL){
		printf("Error reading %s\n", myfilename); 
		return(0);
	}
	while ((ch = fgetc(myfile)) != '\n'){
		if (ch == ',')
			numCol++;
	}
	numCol++;
	return numCol;
}
/*-----------------------------------------------------------------------
@Description:
// extrae los pesos de .csv
@Arguments:

@returns:

@Template:

-------------------------------------------------------------------------*/
int get_weights(char *myfilename, int *Buffer){
	FILE *myfile;
	int ch = 0;
	int idx1 = 0;
	int numRows;
	char tmpStr[900];
	int i;


	myfile = fopen(myfilename, "r");
	if (myfile == NULL){
		printf("Error reading %s\n", myfilename); 
		return(0);
	}
	// lee primera linea
	while ((ch = fgetc(myfile)) != 'N');
	// cuenta el numero de lineas para saber cuantas
	// debo leer
	numRows = countlines(myfilename);

	numRows -= 1;
	for (i = 0; i<numRows; i++) {
		//leo la linea entera
		fscanf(myfile, "%s\n", (char *)&tmpStr);
		//toma el primer Campo que no tiene informacion
		char *token = strtok(tmpStr, ",");
		token = strtok(NULL, ",");// toma el siguiente
		while (token != NULL){
			// convierto a entero 
			Buffer[idx1] = atoi(token);
			token = strtok(NULL, ",");
			idx1++;
		}
	}
	return idx1;
}
/*-----------------------------------------------------------------------
@Description:
// extrae INFORMACION del fichero de pesos.csv
// lee la cabecera del fichero y guarda los datos
// en Buffer
// Ejemplo : Head,3,24,48,24,1,
// idx		Significado
//--------------------------
// 0		N_layers
// 1		n_inputs
// 2		n_neurons layer 1
// 3		n_neurons layer n
//
@Arguments:

@returns:

@Template:

-------------------------------------------------------------------------*/
int get_weights_info (char *myfilename, int *Buffer){
	FILE *myfile;
    int idx1 = 0;	
	char tmpStr[100];

    myfile = fopen(myfilename, "r");
    if (myfile == NULL){
        printf("Error reading %s\n", myfilename);
        return(0);
    }
	// lee primera linea
	fscanf(myfile, "%s\n", (char *)&tmpStr);

	//toma el primer Campo que no tiene informacion
	char *token = strtok(tmpStr, ",");
	token = strtok(NULL, ",");// toma el siguiente
	while(token != NULL){			
		// convierto a entero 
		Buffer[idx1] = atoi(token);			
		token = strtok(NULL, ",");
		idx1++;
	}
	return idx1;		
}

