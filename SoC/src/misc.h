
#include "stdio.h"
#include "stdint.h"
/*-----------------------------------------------------------------------
@Description:

@Arguments:

@returns:

@Template:

-------------------------------------------------------------------------*/
void clearBuffer(int *buffer, int size){
	int i;
	for (i = 0; i < size; i++){
		buffer[i] = 0;
	}
}
void print_buffer_uint8(uint8_t * buffer, size_t buf_len)
{
    uint16_t i;
    printf("buffer = ");
    for (i = 0; i < buf_len; i++)
    {
        printf("%u,", buffer[i]);
    }
    printf("\r\n");
    
}
/*-----------------------------------------------------------------------
@Description:

@Arguments:

@returns:

@Template:

-------------------------------------------------------------------------*/
void BufferFill(int *buffer, int size, int value){
	int i;
	for (i = 0; i < size; i++){
		buffer[i] = value;
	}
}
/*-----------------------------------------------------------------------
@Description:
    convierto a entero con signo a partir de el valor devuelto por fpga
@Arguments:

@returns:

@Template:

-------------------------------------------------------------------------*/
int16_t conv_int_signed(uint16_t number, uint8_t res){
	int16_t int_sig;
	int16_t half_res, full_res;

// printf("");
// printf("conv_int_signed :\r\n");
// printf("- number = %d\r\n", number);

	half_res = 1 << (res - 1);
// printf("- half_res = %d\r\n", half_res);
	full_res = half_res | (half_res - 1);
// printf("- full_res = %d\r\n", full_res);
// printf("");
	if (number >= half_res){
		int_sig = (full_res + 1 - number)*-1;
	}
	else
		int_sig = number;
	return int_sig;
}


// haya le minimo valor posible en un entero con signo con la resolucion
// en bits especificada.
// asi por ej: para res=8 , el minimo valor es -128.
int16_t minValue_signed(uint8_t res){
	int half_res,full_res;
	int int_sig;

	half_res = 1 << (res-1);
	full_res = half_res | (half_res-1);	
	int_sig = (full_res + 1 - half_res)*-1;
	
	return int_sig;
}

// Convierte a one hot las salidas signed que me da el mlp.
uint16_t neuron_out_to_one_hot(uint16_t * data_return, uint16_t numCtg, uint8_t res_bits, uint8_t * one_hot_buffer){
	uint16_t i;
	int16_t max = minValue_signed(res_bits); // empieza valiendo lo minimo
	uint16_t mask;
	uint16_t result;

	// haya el maximo valor de todos los resultados...y[i]
// printf("y = [");
	for (i=0; i < numCtg ; i++){
			data_return[i] = conv_int_signed(data_return[i], res_bits);
// printf("%d, ", (int16_t)data_return[i]);
			if ((int16_t)(data_return[i]) > max)
				max = (data_return[i]);
	}
// printf("], max = %d\n", max);

	// convierte a one-hot
	// si el numero de categorias es > 1. Sino le aplica una funcion de 
	// step.
	if(numCtg>1){
		mask = 1 << (numCtg-1);
		result = 0;
		for (i=0; i < numCtg ; i++){
				if((data_return[i]) == max){
					one_hot_buffer[i] = 1;	
					result |= mask;
				}
				else one_hot_buffer[i] = 0;		
				mask >>= 1;
		} 
	}
	else{// step function
		if (data_return[0] > 0) result = 1;
		else result = 0;
		one_hot_buffer[0]=result;
	}

// print_buffer_uint8(one_hot_buffer, 10);

//printf("data_return = %d, ",data_return[0]);
//printf("result = %d\n",result);
	return result;
}



/*
	convierte on hot binario (el que devuelve FPGA) a categoria entero

*/		
int one_hot_bin_to_catg_int(int ohbin , int numCatg){
	int catgint=0;
	int mask = 1;
	int cont_ones = 0;
	int i;

	mask <<= (numCatg-1);

	for (i=0 ; i<numCatg ; i++){
		if (ohbin == mask){
			catgint = i; // la categoria es el indx + 1
			cont_ones++;
		}
		mask>>=1;
	}

	if (cont_ones != 1) // por seguridad no debe dispararse mas de 1 neruonas de salida
		catgint = -1;
	//sprintf(str, "%b", ohbin);

	return catgint;
}