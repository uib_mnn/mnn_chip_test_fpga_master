/*******************************************************************************************
* INCLUDES
********************************************************************************************/
#include "platform_header.h"

// Select where is gonna be running
//#define RUN_IN_WINDOWS
#define RUN_IN_LINUX

#ifdef RUN_IN_LINUX

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/ipc.h> 
#include <sys/shm.h> 
#include <sys/mman.h>
#include <sys/time.h> 
#include <math.h> 
#include <inttypes.h>

// #include "Input/model.h" //generado por python
#include "hps_0.h"
#include "define_axi.h"
#include "log.h"

//#include "report.h"
// #include "my_timer.h"
// #include "misc.h"
// #include "file_mng.h"

/*******************************************************************************************
* PRIVATE MACROS
********************************************************************************************/

/********************************************************************************************
* PRIVATE TYPES
********************************************************************************************/
/********************************************************************************************
* PRIVATE VARIABLES
********************************************************************************************/
// define ports to comunicate with FPGA, declared in qsys
volatile unsigned int  *i_fsm_cmnd_wr = NULL;
volatile unsigned int  *i_fsm_cmnd_ctrl = NULL;
volatile unsigned int  *o_fsm_busy = NULL;
volatile unsigned int  *o_fsm_cmnd_rd = NULL;
volatile unsigned int  *i_fsm_add = NULL;
volatile unsigned int  *i_fsm_data_wr = NULL;
volatile unsigned int  *o_fsm_data_rd = NULL;
volatile unsigned int  *i_fsm_wr_en = NULL;
volatile unsigned int  *o_fsm_aux1n = NULL;
volatile unsigned int  *o_fsm_aux2n = NULL;
volatile unsigned int *led = NULL;
/********************************************************************************************
* PUBLIC VARIABLES
********************************************************************************************/
/********************************************************************************************
* PRIVATE FUNCTIONS
********************************************************************************************/
/********************************************************************************************
* PUBLIC FUNCTIONS
********************************************************************************************/
void platform_init(void){
    void *virtual_base;
	int fd;	

	// map the address space for the LED registers into user space so we can interact with them.
	// we'll actually map in the entire CSR span of the HPS since we want to access various registers within that span
	if( ( fd = open( "/dev/mem", ( O_RDWR | O_SYNC ) ) ) == -1 ) {
		LOG( "ERROR: could not open \"/dev/mem\"...\n" );
		//return( 1 );
	}

	virtual_base = mmap( NULL, HW_REGS_SPAN, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, HW_REGS_BASE );

	if( virtual_base == MAP_FAILED ) {
		LOG( "ERROR: mmap() failed...\n" );
		close( fd );
		//return( 1 );
	}	
	// Pointer assigment for hps ports
	// include here all ports declared in qsys
	i_fsm_cmnd_wr = GET_POINTER_TO_LIGHT_AXI(virtual_base, PIO_FSM_CMND_WR_BASE);
	i_fsm_cmnd_ctrl = GET_POINTER_TO_LIGHT_AXI(virtual_base, PIO_FSM_CMND_CTRL_BASE);
	o_fsm_busy = GET_POINTER_TO_LIGHT_AXI(virtual_base, PIO_FSM_BUSY_BASE);
	o_fsm_cmnd_rd = GET_POINTER_TO_LIGHT_AXI(virtual_base, PIO_FSM_CMND_RD_BASE);
	i_fsm_add = GET_POINTER_TO_LIGHT_AXI(virtual_base,PIO_FSM_ADD_BASE );
	i_fsm_data_wr = GET_POINTER_TO_LIGHT_AXI(virtual_base, PIO_FSM_DATA_WR_BASE);
	o_fsm_data_rd = GET_POINTER_TO_LIGHT_AXI(virtual_base, PIO_FSM_DATA_RD_BASE);
	i_fsm_wr_en = GET_POINTER_TO_LIGHT_AXI(virtual_base,PIO_FSM_WR_EN_BASE );
	o_fsm_aux1n = GET_POINTER_TO_LIGHT_AXI(virtual_base, PIO_FSM_AUX1N_BASE);
	o_fsm_aux2n = GET_POINTER_TO_LIGHT_AXI(virtual_base, PIO_FSM_AUX2N_BASE);
	led = GET_POINTER_TO_LIGHT_AXI(virtual_base, LED_PIO_BASE);
}

void platform__gpio_set_clk_freq(uint8_t value)
{
    *i_fsm_cmnd_ctrl = value;    
}
void platform__gpio_wr_data(uint8_t value)
{
    *i_fsm_data_wr = value;
}
void platform__gpio_wr_wren(uint8_t value)
{
    *i_fsm_wr_en = value;
}
void platform__gpio_wr_add(uint8_t value)
{
    *i_fsm_add = value;
}
void platform__gpio_wr_ctrl(uint8_t value)
{
    *i_fsm_cmnd_wr = value;
}
uint32_t platform__gpio_rd_data_result(void)
{
    return *o_fsm_data_rd;
}
bool platform__gpio_rd_o_done(void)
{
    return *o_fsm_busy;
}


void platform_end(void){
    LOG("\n\n[End] press any key to finish...");  
    char tmpchar;
    scanf("%c",&tmpchar);   
}

#else // RUN_IN_WINDOWS


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "log.h"
/*******************************************************************************************
* PRIVATE MACROS
********************************************************************************************/
#define TAG "[platform] "

// #define PLATFORM_EN_LOG

#ifdef PLATFORM_EN_LOG
#define PLATFORM_LOG(...)	LOG(__VA_ARGS__)
#define PLATFORM_LOGLN(...)	LOGLN(__VA_ARGS__)
#else
#define PLATFORM_LOG(...)	
#define PLATFORM_LOGLN(...)	

#endif
/********************************************************************************************
* PRIVATE TYPES
********************************************************************************************/
/********************************************************************************************
* PRIVATE VARIABLES
********************************************************************************************/
// define ports to comunicate with FPGA, declared in qsys
volatile unsigned int  *i_fsm_cmnd_wr;
volatile unsigned int  *i_fsm_cmnd_ctrl;
volatile unsigned int  *o_fsm_busy;
volatile unsigned int  *o_fsm_cmnd_rd;
volatile unsigned int  *i_fsm_add;
volatile unsigned int  *i_fsm_data_wr;
volatile unsigned int  *o_fsm_data_rd;
volatile unsigned int  *i_fsm_wr_en;
volatile unsigned int  *o_fsm_aux1n;
volatile unsigned int  *o_fsm_aux2n;
volatile unsigned int *led;
/********************************************************************************************
* PUBLIC VARIABLES
********************************************************************************************/
/********************************************************************************************
* PRIVATE FUNCTIONS
********************************************************************************************/
/********************************************************************************************
* PUBLIC FUNCTIONS
********************************************************************************************/
void platform_init(void){
    
	PLATFORM_LOGLN(TAG"platform_init");  
}

void platform__gpio_set_clk_freq(uint8_t value)
{
    // *i_fsm_cmnd_ctrl = (unsigned int)value;  
	PLATFORM_LOGLN(TAG"platform__gpio_set_clk_freq = %u", value);  
}
void platform__gpio_wr_data(uint8_t value)
{
    // *i_fsm_data_wr = value;
	PLATFORM_LOGLN(TAG"platform__gpio_wr_data = %d", (int8_t)value);  
}
void platform__gpio_wr_wren(uint8_t value)
{
    // *i_fsm_wr_en = value;
	PLATFORM_LOGLN(TAG"platform__gpio_wr_wren = %u", value);  
}
void platform__gpio_wr_add(uint8_t value)
{
    // *i_fsm_add = value;
	PLATFORM_LOGLN(TAG"platform__gpio_wr_add = %u", value);  
}
void platform__gpio_wr_ctrl(uint8_t value)
{
    // *i_fsm_cmnd_wr = value;
	PLATFORM_LOGLN(TAG"platform__gpio_wr_ctrl = %u", value);  
}
uint32_t platform__gpio_rd_data_result(void)
{
    static uint32_t data = 100;
    PLATFORM_LOGLN(TAG"platform__gpio_rd_data_result = %u", data);  
    return (uint32_t) data++; // for testing
}
bool platform__gpio_rd_o_done(void)
{
	usleep(5000); // 5 ms waiting o_done	
	return true;
}


void platform_end(void){
    LOGLN("\n\n[End] press any key to finish...");  
    char tmpchar;
    scanf("%c",&tmpchar);   
}


#endif




