/*******************************************************************************************
* INCLUDES
********************************************************************************************/
#include "init_file.h"
#include "log.h"
#include "stdio.h"

/*******************************************************************************************
* PRIVATE MACROS
********************************************************************************************/
#define FILE_NAME_INIT      	"Input/init.txt"

/********************************************************************************************
* PRIVATE VARIABLES
********************************************************************************************/


/********************************************************************************************
* PUBLIC VARIABLES
********************************************************************************************/

/********************************************************************************************
* PRIVATE FUNCTIONS
********************************************************************************************/

/********************************************************************************************
* PUBLIC FUNCTIONS
********************************************************************************************/
bool init_file__init(init_file_t * init_file)
{
    char tmpStr[128];

    FILE *file_init = fopen(FILE_NAME_INIT, "r");
    fscanf(file_init, "%s %u\n", tmpStr, (unsigned int *)&(init_file->freq_clk));
    fscanf(file_init, "%s %u\n", tmpStr, (unsigned int *)&(init_file->n_samples));
    fscanf(file_init, "%s %s\n", tmpStr, init_file->test_set_path);
    fscanf(file_init, "%s %s\n", tmpStr, init_file->test_set_y_ctg_path);
    // //fscanf(file_init, "%s %u\n", tmpStr, &lfsr2_seed);
    // fscanf(file_init, "%s %u\n", tmpStr, &n_samples);
    // fscanf(file_init, "%s %u\n", tmpStr, &numCtg);
    // fscanf(file_init, "%s %s\n", tmpStr, filename_x_sc);
    // fscanf(file_init, "%s %s\n", tmpStr, filename_weights_sc);
    // //fscanf(file_init, "%s %s\n", tmpStr, filename_fpga_out);
    return true;
} 
void init_file__print(init_file_t * init_file)
{
    LOG("Reading Init file" LOG_EOL);
    LOG("--------------------" LOG_EOL);
    LOG(LOG_TAB "freq_clk = %u, %s" LOG_EOL, init_file->freq_clk, chip32n__get_freq_clk_str(init_file->freq_clk));
    LOG(LOG_TAB "n_samples = %u" LOG_EOL, init_file->n_samples);
    LOG(LOG_TAB "test_set_path = %s" LOG_EOL, init_file->test_set_path);
    LOG(LOG_TAB "test_set_y_ctg_path = %s" LOG_EOL, init_file->test_set_y_ctg_path);
    LOG("" LOG_EOL);
}

